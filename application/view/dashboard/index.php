<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["calendar"]});
      google.charts.setOnLoadCallback(drawChart);

   function drawChart() {
       var dataTable = new google.visualization.DataTable();
       dataTable.addColumn({ type: 'date', id: 'Date' });
       dataTable.addColumn({ type: 'number', id: 'Won/Loss' });

       dataTable.addRows([
           <?php foreach ($this->vacations as $item) { 
               ?>
        
          [ new Date('<?=str_replace('-', ',', explode(' ', $item['date'])[0 ])?>'), -1],

        <?php } ?>
        ]);

       var chart = new google.visualization.Calendar(document.getElementById('calendar_basic'));

       var options = {
         title: "Urlaub",
         height: 600,
       };

       chart.draw(dataTable, options);
   }
    </script>
<div id="calendar_basic" style="width: 1000px; height: 350px;"></div>
