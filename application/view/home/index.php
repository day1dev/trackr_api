<!--
    Denis Karpoukhine
    UKE, GB-IT
    2019                
-->
<!-- TODO: evtl class einfügen -->
<section class="">
    <div class="">
        <h3>Netzwerk</h3>
        <div class="row my-md-2">
            <div class="col">
                <label for="netz" class="left">Netz<i class="req">*</i></label>
                <select id="netz" name="netz" class="form-control" required>
                    <!-- Combobox mit den Netzen (Möglich dynamisch mit PHP) -->
                    <option value="-1">Netz Auswählen...</option>
                    <option value="1">KIS1</option>
                    <option value="2">KIS2</option>
                    <option value="4">GWIS</option>
                    <option value="5">Abteilungsnetz</option>
                </select>
				<div id="abteilungsnetz" class="col-md-4" hidden>
				<label for="netz_bez" class="left">Abteilungsnetz Bezeichnung<i class="req">*</i></label>
				<input type="text" name="netz_bez" class="form-control" id="netz_bez" placeholder="Bitte eingeben..." />
				</div>
			</div>
        </div>
        <div id="view-geraete" hidden>
            <hr />
            <h3>Geräte</h3>
            <!-- Aktivierung von Spalten mit Forms -->
            <div class="row">
                <div class="col">
                <!-- Checkbox Neugerät PC, Laptop -->
                    <input type="checkbox" name="cb-pc" id="cb-pc" value="true" />
                    <label class="form-check-label" for="cb-pc">Laptop/PC Neugerät</label>
                </div>
                <!-- Checkbox Monitor Neugerät -->
                <div class="col">
                    <input type="checkbox" name="cb-mon" id="cb-mon" value="true" />
                    <label class="form-check-label" for="cb-mon">Monitor Neugerät</label>
                </div>
                <!-- Checkbox Altgeräte Spalte -->
                <div class="col">
                    <input type="checkbox" name="cb-alt" id="cb-alt" value="true" />
                    <label class="form-check-label" for="cb-alt">Altgeräte (Verschrottung, Rückgabe)</label>
                </div>
                <!-- Checkbox Canon Drucker -->
                <div class="col">
                    <input type="checkbox" name="cb-canon-drucker" id="cb-canon-drucker" value="canon-drucker" />
                    <label class="form-check-label" for="cb-canon-drucker">Canon Drucker</label>
                </div>
            </div>
            <div class="row my-md-2">
                <div class="col">
                    <div id="view-pc" hidden>
                        <!-- Textbox UKE Nummer PC neu -->
                        <!-- <h4>PC/Laptop Neugerät</h4> -->
                        <!-- Combobox Gerätetyp -->
                        <label for="pc_typ" class="left">Gerätetyp<i class="req">*</i></label>
                        <select id="pc_typ" name="pc_typ" class="form-control" required>
                        <!-- TODO: PHP code zum parsen -->
                            <option value="-1">Gerätetyp Auswählen...</option>
                            <option value="1">PC</option>
                            <option value="2">Laptop</option>
                        </select>
                        <!-- Feld UKE NUmmer neugerät -->
                        <label for="pc_uke_nr_neu" class="left">UKE Nummer<i class="req">*</i></label>
                        <input value="111111" id="pc_uke_nr_neu" name="pc_uke_nr_neu" class="form-control" type="text" placeholder="UKE Nummer" maxlength="20" required />
                        <!-- Textbox Modell -->
                        <label for="pc_modell" class="left">Modell<i class="req">*</i></label>
                        <input value="640pro" id="pc_modell" name="pc_modell" class="form-control" type="text" placeholder="Modell" maxlength="20" required />
                        <!-- Textbox MAC -->
                        <label for="pc_mac" class="left">MAC Adresse<i class="req">*</i> (z.B. 0F:0A:95:9D:68:B6)</label>
                        <input value="a4:d2:7h:35:ss" id="pc_mac" name="pc_mac" class="form-control" type="text" placeholder="MAC Adresse" maxlength="20" required />
                        <!-- Textbox Seriennr. -->
                        <label for="pc_seriennr" class="left">Seriennummer<i class="req">*</i></label>
                        <input value="f5e54f5e56c2" id="pc_seriennr" name="pc_seriennr" class="form-control" type="text" placeholder="Seriennummer" maxlength="20" required />
                    </div>
                </div>
                <!-- Monitor Neugerät Spalte -->
                <div class="col">
                    <div id="view-mon" hidden>
                        <!-- <h4>Monitor Neugerät</h4> -->
                        <!-- Textbox UKE Nummer PC neu -->
                        <label for="mon_uke_nr_neu" class="left">UKE Nummer<i class="req">*</i></label>
                        <input value="7787887" id="mon_uke_nr_neu" name="mon_uke_nr_neu" class="form-control" type="text" placeholder="UKE Nummer Neugerät Monitor" maxlength="20" required />
                        <!-- Textbox Modell -->
                        <label for="mon_modell" class="left">Modell<i class="req">*</i></label>
                        <input value="einmon" id="mon_modell" name="mon_modell" class="form-control" type="text" placeholder="Modell" maxlength="20" required />
                    </div>
                </div>
                <!-- Altgeräte Spalte -->
                <div class="col">
                    <div id="view-alt" hidden>
                        <!-- <h4>Altgeräte</h4> -->
                        <!-- Textbox UKE Nummer PC alt -->
                        <label for="alt_pc_uke_nr" class="left">UKE Nummer Altgerät PC/Laptop<i class="req">*</i></label>
                        <input value="1245487" id="alt_pc_uke_nr" name="alt_pc_uke_nr" class="form-control" type="text" placeholder="UKE Nummer Altgerät PC/Laptop" maxlength="20" required />
                        <!-- Textbox UKE Nummer Monitor alt -->
                        <label for="alt_mon_uke_nr" class="left">UKE Nummer Altgerät Monitor<i class="req">*</i></label>
                        <input value="7454788" id="alt_mon_uke_nr" name="alt_mon_uke_nr" class="form-control" type="text" placeholder="UKE Nummer Altgerät Monitor" maxlength="20" required />
                        <!-- checkbox Canon Drucker -->
                    </div>
                </div>
                <div class="col">
                    <div id="view-canon-drucker" hidden>
                        <!-- Feld Drucker PR Nummer -->
                        <label for="pr_nr" class="left">PR Nummer<i class="req">*</i> (z.B. PR0003)</label>
                        <input value="PR0001" id="pr_nr" name="pr_nr" class="form-control" type="text" placeholder="PR Nummer" maxlength="20" required />
                        <!-- Feld CANON Drucker Modell -->
                        <label for="pr_modell" class="left">Modell<i class="req">*</i></label>
                        <input value="eindrucker" id="pr_modell" name="pr_modell" class="form-control" type="text" placeholder="Modell" maxlength="20" required />
                    </div>
                </div>
            </div>
        </div>
        <div id="view-ang-geraete" hidden>
            <hr />
            <h3>Angebundene Geräte</h3>
            <div class="ang-geraete">
            <!-- Hier werden weitere Comboboxen eingefügt -->
                <div class="row ang-geraet">
                <!-- Combobox mit der Software -->
                    <div class="col">
                        <label for="ang_geraet_uke_nr" class="left">UKE Nummer<i class="req">*</i></label>
                        <input name="ang_geraet_uke_nr" class="ang_geraet_uke_nr form-control" type="text" placeholder="UKE Nummer" maxlength="20" required />
                    </div>
                    <div class="col">
                        <label for="ang_geraet_bez" class="left">Gerätebezeichnung<i class="req">*</i></label>
                        <input name="ang_geraet_bez" class="ang_geraet_bez form-control" type="text" placeholder="Gerätebezeichnung" maxlength="20" required />
                    </div>
                    <div class="col">
                        <label for="ud_nr" class="left">UD Nummer</label>
                        <input name="ud_nr" class="ud_nr form-control" type="text" placeholder="UD Nummer" maxlength="20" />
                    </div>
                    <!-- Button Add Software -->
                    <div class="col">
                        <br />
                        <button class=" mt-md-2 btn btn-success add-ang-geraet">+</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="view-software" hidden>
            <hr />
            <h3>Installierte Software</h3>
            <div class="software">
            <!-- Hier werden weitere Comboboxen eingefügt -->
                <div class="row" id="software-area">
                <!-- Combobox mit der Software -->
                    <div class="col software_neu">
                        <!-- <select name="software_neu" class="software_bez form-control" required>
                            <option value="-1">Software Auswählen...</option> -->
                            <!-- TODO: PHP code zum parsen -->
                            <!-- <option value="1">Photoshop CC 2019</option>
                            <option value="2">Microsoft Office 2019</option>
                        </select> -->
                        <label for="software_neu" class="left">Software</label>
                        <input value="Microsoft Word" name="software_neu" class="software_bez form-control" type="text" placeholder="Software" maxlength="20" />
                    </div>
                    <!-- Button Add Software -->
                    <div class="col">
                        <br />
                        <button class="mt-md-2 btn btn-success add-software">+</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="view-person" hidden>
            <hr />
            <h3>Benutzer</h3>
            <div class="row">
                <!-- Feld Vorname -->
                <div class="col">
                    <label for="vorname" class="left">Vorname<i class="req">*</i></label>
                    <input value="John" id="vorname" name="vorname" class="form-control" type="text" placeholder="Vorname" maxlength="20" required />
                </div>
                <!-- Feld Nachname -->
                <div class="col">
                    <label for="nachname" class="left">Nachname<i class="req">*</i></label>
                    <input value="Doe" id="nachname" name="nachname" class="form-control" type="text" placeholder="Nachname" maxlength="20" required />
                </div>
                <!-- Feld Telefon -->
                <div class="col">
                    <label for="telefon" class="left">Telefon<i class="req">*</i></label>
                    <input value="01234567" id="telefon" name="telefon" class="form-control" type="text" placeholder="Telefon" maxlength="20" required />
                </div>
                <!-- Feld Email -->
                <div class="col">
                    <label for="email" class="left">E-Mail<i class="req">*</i></label>
                    <input value="john@Doe.com" id="email" name="email" class="form-control" type="text" placeholder="E-Mail" maxlength="20" required />
                </div>
            </div>
            <div class="row">
                <!-- Feld Klinik/Institut -->
                <div class="col">
                    <label for="klin_inst" class="left">Klinik/Institut<i class="req">*</i></label>
                    <input value="GB-IT" id="klin_inst" name="klin_inst" class="form-control" type="text" placeholder="Klinik/Institut" maxlength="20" required />
                </div>
                <!-- Feld Gebäude -->
                <div class="col">
                    <label for="gebaeude" class="left">Gebäude<i class="req">*</i> (z.B. W29, ABK, etc.)</label>
                    <input value="W29" id="gebaeude" name="gebaeude" class="form-control" type="text" placeholder="Gebaeude" maxlength="20" required />
                </div>
                <!-- Feld Etage -->
                <div class="col">
                    <label for="etage" class="left">Etage<i class="req">*</i></label>
                    <!-- <input id="etage" name="etage" class="form-control" type="text" placeholder="Etage" maxlength="20" required /> -->
                    <select value="4" id="etage" name="etage" class="form-control" required>
                        <option value="-1">Etage Auswählen...</option>
                        <option value="1">KG</option>
                        <option value="2">SG</option>
                        <option value="3">UG</option>
                        <option value="4">EG</option>
                        <option value="5">1.OG</option>
                        <option value="6">2.OG</option>
                        <option value="7">3.OG</option>
                        <option value="8">4.OG</option>
                        <option value="9">5.OG</option>
                        <option value="10">6.OG</option>
                        <option value="11">7.OG</option>
                        <option value="12">8.OG</option>
                    </select>
                </div>
                <!-- Feld Raum -->
                <div class="col">
                    <label for="raum" class="left">Raum<i class="req">*</i></label>
                    <input value="21" id="raum" name="raum" class="form-control" type="text" placeholder="Raum" maxlength="20" required />
                </div>
            </div>
        </div>
        <!-- Button Anlegen -->
        <div id="view-button" hidden>
            <hr />
            <div class="row my-md-4">
                <div class="col">
                    <button class="btn btn-lg btn-primary btn-block" type="Submit" name="submit" id="submit">Anlegen</button>
                </div>
                <div class="col-auto">
                    <button class="btn btn-lg btn-danger btn-block" type="Submit" name="clear" id="clear">Alle Felder Leeren</button>
                </div>
            </div>
        </div>
        <!-- STATUS ANZEIGE -->
        <div id="execution-status"></div>
    </div>
</section>

<script src="<?=URL?>js/ajax/home.js"></script>