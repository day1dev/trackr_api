</main>
<footer class="footer light">
    <div class="container">
        <span class="text-muted">&copy; UKE <?=date("Y")?> - Denis Karpoukhine</span>
    </div>
</footer>

<script src="<?=URL?>js/popper.min.js"></script>
<script src="<?=URL?>js/bootstrap.min.js"></script>
<script src="<?=URL?>js/fa.all.min.js"></script>

</body>
</html>