<!DOCTYPE html>
<html>
<head>
    <!-- ALLE WICHTIGEN HEADER TAGS FÜR JEDE SEITE -->
    <link rel="stylesheet" href="<?=URL?>css/google.nanum.gothic.css">
    <link rel="stylesheet" href="<?=URL?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=URL?>css/fancybox/jquery.fancybox.min.css">
    <link rel="icon" type="image/png" href="<?=URL?>img/icon.png" sizes="32x32">
    <link rel="stylesheet" href="<?=URL?>css/style.css">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <!--meta http-equiv="Content-Type" content="text/html; charset=utf-8"-->
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CMDB Webapp</title>
</head>
<body>

<!-- ALLE WICHTIGEN SCRIPTS -->
<script>
        var url = "<?=URL?>";
    </script>
<script src="<?=URL?>js/jquery.min.js"></script>
<script src="<?=URL?>js/utils.js"></script>
<script src="<?=URL?>js/fancybox/jquery.fancybox.min.js"></script>
<header class="pb-4">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-light fixed-top bg-light">
        <a class="navbar-brand" href="#"><img src="<?=URL?>img/logo.svg" height="32"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                
                    <li class="nav-item">
                        <a class="nav-link" href="<?=URL?>home"><span class="fas fa-file-invoice"></span> Formular</a>
                    </li>
                    <!--li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="fa fa-user-tie"></span>
						</a>
                        
                    </li-->
            </ul>
            <!--form class="form-inline mt-2 mt-md-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form-->
            
        </div>
    </nav>
</header>
<main class="px-4 py-5"> <!-- class+ col-md-9 col-lg-10 -->
    <script>
        $(function(){

            var pathname = (window.location.pathname.match(/[^\/]+$/)[0]);

            $('#navbarCollapse ul li a').each(function() {
                if ($(this).attr('href') == pathname)
                {
                    $(this).addClass('active');
                }
            });
        });
    </script>
