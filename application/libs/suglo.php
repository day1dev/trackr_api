
<?php
/**
 * This is under development. Expect changes!
 * Class Request
 * Abstracts the access to $_GET, $_POST and $_COOKIE, preventing direct access to these super-globals.
 * This makes PHP code quality analyzer tools very happy.
 * @see http://php.net/manual/en/reserved.variables.request.php
 */
class Suglo
{
    /**
     * Gets/returns the value of a specific key of the POST super-global.
     * When using just Request::post('x') it will return the raw and untouched $_POST['x'], when using it like
     * Request::post('x', true) then it will return a trimmed and stripped $_POST['x'] !
     *
     * @param mixed $key key
     * @param bool $clean marker for optional cleaning of the var
     * @return mixed the key's value or nothing
     */
    public static function post($key = null, $clean = false)
    {
        if ($key == null) {
            return $_POST;
        } else if (isset($_POST[$key])) {
            // we use the Ternary Operator here which saves the if/else block
            // @see http://davidwalsh.name/php-shorthand-if-else-ternary-operators
            return ($clean) ? trim(strip_tags($_POST[$key])) : $_POST[$key];
        }
    }
    /**
     * Returns the state of a checkbox.
     *
     * @param mixed $key key
     * @return mixed state of the checkbox
     */
    public static function postCheckbox($key)
    {
        return isset($_POST[$key]) ? 1 : NULL;
    }
    /**
     * gets/returns the value of a specific key of the GET super-global
     * @param mixed $key key
     * @return mixed the key's value or nothing
     */
    public static function get($key = null)
    {
        if ($key == null) {
            return $_GET;
        } else if (isset($_GET[$key])) {
            return $_GET[$key];
        }
    }
	
	public static function auto($key = null) {
		if (isset($_GET[$key])) {
			return $_GET[$key];
		} else if (isset($_POST[$key])) {
			return $_POST[$key]; 
		}
	}
    /**
     * gets/returns the value of a specific key of the COOKIE super-global
     * @param mixed $key key
     * @return mixed the key's value or nothing
     */
    public static function cookie($key = null, $value = null, $expire = 0, $path = URL)
    {
        if ($key == null) {
            return $_COOKIE;
        } else if ($value != null) {
            setcookie($key, $value, $expire, $path);
        } else if (isset($_COOKIE[$key])) {
            return $_COOKIE[$key];
        }
    }

    /**
     * gets/returns the value of a specific key of the SESSION super-global
     * @param mixed $key key
     * @return mixed the key's value or nothing
     */
    public static function session($key = null, $value = null) {
        if ($key == null) {
            return $_SESSION;
        } else if ($value != null) {
            $_SESSION[$key] = $value;
        } else if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
    }
}