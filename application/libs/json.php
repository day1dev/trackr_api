<?php

class JSON {
    public $type;
    public $code;
    public $message;
	public $data;

    public function __construct($data = [], $type = 'success', $code = 0, $message = 'Exit without errors.') {
        $this->type = $type;
        $this->code = $code;
        $this->message = $message;
		$this->data = $data;
        $this->print();
    }
	
	public function build() {
		$json = [];

		$json['information'] = [
			'type' => $this->type,
			'code' => $this->code,
			'message' => $this->message,
		];
		
		$json['data'] = $this->data;

		$json = json_encode($json);#, JSON_UNESCAPED_UNICODE);#, JSON_FORCE_OBJECT);
		
		#echo json_last_error_msg();
		return $json;
	}

    public function print() {
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json; charset=utf-8');
		
        echo $this->build();
    }
}