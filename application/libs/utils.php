<?php

class Utils {
	public static function getRealIp() {
		$ip = '0.0.0.0';
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
		  $ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	//creates a string of a given length
    static function createString($len, $uppercase = false) {
        $string = "1qay2wsx3edc4rfv5tgb6zhn7ujm8ik9olpAQWSXEDCVFRTGBNHYZUJMKILOP";
		$newString = substr(str_shuffle($string), 0, $len);
		return ($uppercase) ? strtoupper($newString) : $newString;
    }
	
	public static function checkIsset($array) {
		if (!is_array($array)) {
			return false;
		}
		foreach ($array as $item) {
			if (!isset($item)) {
				return false;
			}
		}
		return true;
	}

	static function utf8ize($d) {
    if (is_array($d)) {
        foreach ($d as $k => $v) {
            $d[$k] = utf8ize($v);
        }
    } else if (is_string ($d)) {
        return utf8_encode($d);
    }
    return $d;
	}
}