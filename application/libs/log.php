<?php

class Log {
	public static function write($dbh, $text, $parameters, $user_id = 1) {
		return $dbh->query(
			'	INSERT INTO log(content, user_id, parameters)
				VALUES(:content, :user_id, :parameters);
				',
			[
				':content' => $text,
				':user_id' => $user_id,
				':parameters' => json_encode($parameters)
			]
		);
	}
}