<?php

class Controller {
    public $dbh;
	public $auth;
    //public $model;
    public $view;
	public $guest;

    public function __construct($guest = false) {
        require APP . 'classes/dbh.php';
		require APP . 'classes/core_auth.php';
		
        $this->dbh = new Dbh();
		$this->auth = new Auth($this->dbh);
		
		if (!$this->auth->authenticate($guest)) {
			exit;
		}
        //$this->loadModel();
        $this->loadView();
    }
	
	//ursprünglich gedacht für anmeldungslose seiten wie qr code gen
	public function setGuest($bool) {
		$this->guest = $bool;
	}

    public function loadView() {
        require APP . 'core/view.php';
        $this->view = new View(/*, $this->model*/);
    }
	
	/**
     * 
     * @param string $name Name of the model
     * @param string $path Location of the models
     */
    public function loadModel($name, $modelPath = 'model/') {
        $path = APP . $modelPath . $name.'.php';
		//echo $path;
        if (file_exists($path)) {
            require $path; //$modelPath .$name.'.php';
            
            $modelName = $name . 'Model';
			
            return new $modelName();
        }        
    }
}

?>