<?php
	
class Model {
	#damit kann auch innerhalb der klasse rumhantiert werden
    protected $dbh;
	
    function __construct(/*$dbh, $login*/) {
        try {
			//require APP . 'classes/dbh.php';
			//require APP . 'classes/clogin.php';
			
            $this->dbh = new Dbh(); //$dbh;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    #für die erstellung eines elements (zuordnung eines wempfs)
    #parsed die Warenempfänger aus der Datenbank und gibt sie als


    #The following function will strip the script name from URL
    #i.e.  http://www.something.com/search/book/fitzgerald will become /search/book/fitzgerald
    function getCurrentUri(){
        $basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
        $uri = substr($_SERVER['REQUEST_URI'], strlen($basepath));
        if (strstr($uri, '?')) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }
        $uri = '/' . trim($uri, '/');
        return $uri;
    }

    #wird glaube ich garnicht genutzt
    function parseUrl() {
        $base_url = getCurrentUri();
        $routes = explode('/', $base_url);
        foreach($routes as $route){
            if(trim($route) != '') {
                array_push($routes, $route);
            }
        }
        return $routes;
    }

    #prüft den bestand der permission
    function checkPermission($perm) {
        #echo $perm;
        if (!$this->login->validatePermission($perm)) {
            return false;
        } else {
            return true;
        }
    }
	
	
	function download_send_headers($filename) {
		// disable caching
		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");

		// force download  
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");
	}
}