<?php

class Permissions extends Controller {

    public function index() {

    }

    public function read($role_id = null) {
        $permissionsModel = $this->loadModel('permissions');

        if ($role_id == null) {
            $permissions = []; //$permissionsModel->getPermissions();
        } else {
            $permissions = $permissionsModel->getPermissions(
            [ 
                ':role_id' => $role_id,
                ':role_id2' => $role_id
            ]);
        }

        new JSON($permissions);
    }
}