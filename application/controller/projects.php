<?php

class Projects extends Controller {
	
	public function index() {
		
	}
	
	public function create() {
		$this->auth->validatePermission(723, true);

		$post = [
			'name' => suglo::post('project_name'),
			'description' => suglo::post('project_description'),
			'plan_from' => suglo::post('plan_from'),
			'plan_to' => suglo::post('plan_to'),
			'plan_hours' => suglo::post('plan_hours')
		];

		if (!utils::checkIsset($post)) {
			new JSON([], 'error', 1001, 'Invalid or missing paramater.');
			exit;
		}

		$model = $this->loadModel('projects');
		
		$result = $model->insertProject($post);

		if ($result == 1) {
			new JSON([], 'success', 400, 'Project created.');
		} else {
			new JSON([], 'error', 9000, 'Something went wrong.');
		}
	}
	
	public function read($id = null) {
		$this->auth->validatePermission(700, true);
		
		$post = [
			'employee_id' => suglo::post('employee_id'),
			'project_id' => $id
		];

		$model = $this->loadModel('projects');

		if ($post['employee_id'] && $post['project_id']) {
			$projects = $model->getProjects(
				'WHERE employee_id = :employee_id AND project_id = :project_id',
				[
					':employee_id' => $post['employee_id'],
					':project_id' => $post['project_id']
				]
			);
		} else if ($post['employee_id']) {
			$projects = $model->getProjects('WHERE employee_id = :employee_id',
				[
					':employee_id' => $post['employee_id']
				]
			);
		} else if ($post['project_id']) {
			$projects = $model->getProjects('WHERE project_id = :project_id',
				[
					':project_id' => $post['project_id']
				]
			);
		} else {
			$projects = $model->getProjects();
		}
		
		new JSON($projects);
	}
}