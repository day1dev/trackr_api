<?php

class Tracks extends Controller {
	public function index() {
		
	}
	
	public function create() {
		$this->auth->validatePermission(213, true);

		$status_error = false;
		$scanner_is_employee = false;

		$post =  [
			'scanner_uid' => suglo::auto('scanner_uid'),
			'device_uid' => suglo::auto('device_uid'),
			'new_status_id' => suglo::auto('status_id'),
			'project_id' => suglo::auto('project_id'),
			'timestamp' => suglo::auto('timestamp')
		];

		#is mac or device unset? -> terminate
		if (!$post['scanner_uid'] || !$post['device_uid'] || !$post['new_status_id']) {
			new JSON([], 'error', 1001, 'Invalid or missing paramater.');
			exit;
		}

		$tracksModel = $this->loadModel('tracks');
		$devicesModel = $this->loadModel('devices');
		$employeesModel = $this->loadModel('employees');
		
		$employee = $employeesModel->getEmployeeByDevice($post['device_uid']);

		if ($employee['id'] < 1) {
			#get the employee by mac
			$employee = $employeesModel->getEmployeeByDevice($post['scanner_uid']);
			#das scannende gerät ist einem Mitarbeiter zugeordnet
			$scanner_is_employee = true;
		}

		#no employee is using the mac
		if ($employee['id'] < 1) {
			new JSON([], 'error', 2001, 'Employee not found by Device UID.');
			exit;
		}

		#''''''''''''''
		#WAS IST WENN DER EMPLOYEE NICHT IM PROJEKT IST
		#ABER SICH TROTZDEM DARÜBER ANMELDET
		#''''''''''''''

		#get the previous status of a user
		$prev_track = $tracksModel->getPrevTrack($employee['id']);

		switch ($prev_track['status_id']) {
			#neuer status darf nicht dem alten gleich sein
			case $post['new_status_id']:
				$status_error = true;
				break;
			#enter kann nicht break_end folgen
			case 1:
				if ($post['new_status_id'] == 4) {
					$status_error = true;
				}
				break;
			#leave kann nicht break_* folgen
			case 2:
				if ($post['new_status_id'] == 3 || $post['new_status_id'] == 4) {
					$status_error = true;
				}
				break;
			#break_start kann nichts außer break_end folgen
			case 3:
				if ($post['new_status_id'] == 1 || $post['new_status_id'] == 2) {
					$status_error = true;
				}
				break;
			#break_end kann nicht enter folgen
			case 4:
				if ($post['new_status_id'] == 1) {
					$status_error = true;
				}
				break;
			#nach krankheit oder urlaub eines tages kein neuer status am selben tag
			case 5:
				if (date('Y-m-d', strtotime($prev_track['date'])) == date('Y-m-d')) {
					$status_error = true;
				}
				break;
			case 6:
				if (date('Y-m-d', strtotime($prev_track['date'])) == date('Y-m-d')) {
					$status_error = true;
				}
				break;
		}

		if ($status_error) {
			new JSON([], 'error', 3001, 'Illegal status.');
			exit;
		}

		$device = $devicesModel->getDevices('WHERE uid = :device_uid;',
			[
				#wenn der scanner einem mitarbeiter gehört, dann geben wir die gescannte uid
				':device_uid' => $scanner_is_employee ? $post['device_uid'] : $post['scanner_uid']
			]
		)[0];

		if ($device['id'] < 1) {
			new JSON([], 'error', 3002, 'Invalid device.');
			exit;
		}
		
		$post['device_id'] = $device['id'];
		$post['employee_id'] = $employee['id'];
		#insert new track into tracks
		$result = $tracksModel->insertTrack($post);

		if (!$result) {
			new JSON([], 'error', 4001, 'Unable to insert track.');
			exit;
		}
		
		new JSON([], 'success', 101, "$post[new_status_id];$device[name];" . date('Y-m-d H:i:s'));
		exit;
	}

	function read($employee_id = null) {

		$post = [
			'employee_id' => $employee_id,
			'min_date' => suglo::auto('min_date')
		];

		if (!$post['employee_id']) {
			new JSON([], 'error', 1001, 'Invalid or missing paramater.');
			exit;
		}

		if (!$post['min_date']) {
			$post['min_date'] = date('Y').'-01-01';
		}

		if ($post['employee_id'] != $this->auth->employee_id && !$this->auth->validatePermission(200)) {
			new JSON([], 'error', 1002, 'Missing Permission 200.');
			exit;
		}

		$tracksModel = $this->loadModel('tracks');
		$tracks = $tracksModel->getTracks('
			WHERE employees.id = :employee_id
			AND tracks.date >= :min_date
			AND tracks.status_id <= 4',
			[
				':employee_id' => $post['employee_id'],
				':min_date' => $post['min_date']
			]
		);

		new JSON($tracks);
		exit;
	}
}