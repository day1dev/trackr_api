<?php

class Vacations extends Controller {

		public function index() {
			
		}
		
		public function update($id = null) {
			$this->auth->validatePermission(324, true);
			
			$vacationsModel = $this->loadModel('vacations');
			
			$post = [
				'status_id' => suglo::auto('status_id')
			];

			if (!utils::checkIsset($post) || $id == null) {
				new JSON([], 'error', 1001, 'Invalid or missing paramater.');
				exit;
			}

			if ($post['status_id'] != 8 && $post['status_id'] != 9) {
				new JSON([], 'error', 3001, 'Illegal status.');
				exit;
			}

			$result = $vacationsModel->updateVacation(
				[
					'status_id' => $post['status_id']
				],
				'id = '.$id.' AND status_id = 7;'
			);

			if (!$result) {
				new JSON([], 'error', 3025, 'Unable to change status.');
				exit;
			} else {
				new JSON([], 'success', 300, 'Vacation status updated.');
				exit;
			}
		}
		
		public function read($id = null) {
			$vacationsModel = $this->loadModel('vacations');
			$employeesModel = $this->loadModel('employees');
			
			$post = [
				'employee_id' => suglo::auto('employee_id'),
				'device_uid' => suglo::auto('device_uid')
			];

			if ($post['device_uid']) {
				$device_employees = $employeesModel->getEmployeeByDevice($post['device_uid']);
				if (isset($device_employees[0])) {
					$device_employee = $employeesModel->getEmployees(
						' WHERE employees.id = :employees_id ',
						[
							':employee_id' => $device_employees[0]
						]
					);
				}
			}

			if ($post['employee_id']) {
				$employees = $employeesModel->getEmployees(
					' WHERE employees.id = :employees_id ',
					[
						':employee_id' => $post['employee_id']
					]
				);
				if (isset($employees[0])) {
					$employee = $employees[0];
				}
			}

			if ($id != null) {
				if ($this->auth->validatePermission(Permissions::VACATION_VIEW_ALL)) {
					$vacations = $vacationsModel->getVacations(
						' WHERE vacations.id = :id ',
						[
							':id' => $id
						]
					);
				} else {
					$vacations = $vacationsModel->getVacations(
						' WHERE vacations.id = :id 
						  AND (		employees.supervisor_id = :supervisor_id 
						  		OR 	devices.device_uid = :device_uid ) ',
						[
							':id' => $id,
							':supervisor_id' => $device_employee['id'],
							':device_uid' => $post['device_uid']
						]
					);
				}
			} else if ($post['employee_id']) {
				if ($this->auth->validatePermission(Permissions::VACATION_VIEW_ALL)) {
					$vacations = $vacationsModel->getVacations(
						' WHERE vacations.employee_id = :employee_id ',
						[
							':employee_id' => $post['employee_id']
						]
					);
				} else {
					$vacations = $vacationsModel->getVacations(
						' WHERE vacations.employee_id = :employee_id 
						  AND (		employee.id = :supervisor_id
						  		OR	devices.device_uid = :device_uid ) ',
						[
							':employee_id' => $post['employee_id'],
							':supervisor_id' => $device_employee['id'],
							':device_uid' => $post['device_uid']
						]
					);
				}
			} else {
				if ($this->auth->validatePermission(Permissions::VACATION_VIEW_ALL)) {
					$vacations = $vacationsModel->getVacations();
				} else {
					$vacations = $vacationsModel->getVacations(
						' WHERE devices.device_uid = :device_uid ',
						[
							':device_uid' => $post['device_uid']
						]
					);
				}
			}
				
			new JSON($vacations);
		}
		
		public function create() {
			$this->auth->validatePermission(Permissions::VACATION_CREATE_SELF, true);
			
			$employeesModel = $this->loadModel('employees');
			$vacationsModel = $this->loadModel('vacations');

			$post = [
				'date_from' => suglo::auto('date_from'),
				'date_to' => suglo::auto('date_to'),
				'date_json' => suglo::auto('date_json'),
				'device_uid' => suglo::auto('device_uid')
			];

			if ((!$post['date_from'] || !$post['date_to']) && !$post['date_json']) {
				new JSON([], 'error', 1001, 'Invalid or missing paramater.');
				exit;
			}

			if ($post['date_from'] && $post['date_to']) {
				if (strtotime($post['date_from']) > strtotime($post['date_to']) ||
					strtotime($post['date_from']) < strtotime('now')) {
					new JSON([], 'error', 5002, 'Invalid Dates.');
					exit;
				}
			} else if ($post['date_json']) {
				$date_array = json_decode($post['date_json']);
			}

			$employee = $employeesModel->getEmployeeByDevice($post['device_uid']);

			if (!($employee['id'] > 0)) {
				new JSON([], 'error', 2001, 'Employee not found by Device UID.');
				exit;
			}

			if (isset($date_array)) {
				foreach ($date_array as $date) {
					$result = $vacationsModel->insertVacation(
						[
							'employee_id' => $employee['id'],
							'date_from' => $date,
							'date_to' => $date
						]
					);
				}
			} else {
				$result = $vacationsModel->insertVacation(
					[
						'employee_id' => $employee['id'],
						'date_from' => $post['date_from'],
						'date_to' => $post['date_to']
					]
				);
			}

			if (!$result) {
				new JSON([], 'error', 5001, 'Unable to insert vacation.');
			} else {
				new JSON([], 'success', 500, 'Vacation inserted.');
			}
		}
	
}