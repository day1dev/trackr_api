<?php
/**
 * Created by PhpStorm.
 * User: dkarpo
 * Date: 12.09.2018
 * Time: 11:40
 */

class Error404 extends Controller {

    function index() {
        $this->view->render('error/index',
            [
                'code' => E_PAGE_NOT_FOUND,
                'message' => 'Page not found.'
            ]);
    }
}