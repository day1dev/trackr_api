<?php

class Devices extends Controller {
	
	public function index() {
		
	}
	
	public function create() {
		$this->auth->validatePermission(413, true);

		$post = [
			'name' => suglo::post('name'),
			'description' => suglo::post('description'),
			'uid' => suglo::post('uid'),
			'type_id' => suglo::post('type_id'),
		];

		if (!utils::checkIsset($post)) {
			new JSON([], 'error', 1001, 'Invalid or missing paramater.');
			exit;
		}
		
		$model = $this->loadModel('devices');

		$result = $model->insertDevice($post);
		
		if ($result == 1) {
			new JSON([], 'success', 400, 'Device entry created.');
		} else {
			new JSON([], 'error', 9000, 'Something went wrong.');
		}
	}
	
	public function read($id = null) {
		$this->auth->validatePermission(400, true);
		$model = $this->loadModel('devices');

		if ($id != null) {
			#find device by id if isset
			$devices = $model->getDevices('WHERE devices.id = :id', 
				[
					':id' => $id
				]
			);
			
			new JSON($devices);
		} else {
			#gets all devices
			$devices = $model->getDevices();
			
			new JSON($devices);
		}

	}
}