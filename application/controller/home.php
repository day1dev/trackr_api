<?php
/*  Denis Karpoukhine
**  GB-IT, UKE
**  2019
*/
class Home extends Controller {
    public function index() {
        $this->view->render('home/index');
    }

    //ajax Methode
    public function ajaxSubmitForm() {
        $data = suglo::post('data');
        $software = suglo::post('soft');
        $ang_ger = suglo::post('ang_ger');

        $model = $this->loadModel('home');

        foreach ($data as $key => $val) {
            $data[$key] = utils::clearText($val);
        }
        
        $sent = 0;
        $received = 0;
        $messages = [];
		
		if (!is_numeric($data['netz'])) {
			$netz = $model->getLastNetz(
				'WHERE name = \''.$data['netz'].'\''
			)['id'];
			
			if ($netz < 1) {
				$model->insertNetz($data['netz']);
				$netz = $model->getLastNetz(
					'WHERE name = \''.$data['netz'].'\''
				)['id'];
				
				if ($netz < 1) {
					echo json_encode('e:Cannot insert Netz');
				}
			}
			
			$data['netz'] = $netz;
		}

        if ($data['cb_pc']) {
            $geraet = [
                'typ' => $data['pc_typ'],
                'uke_nr' => $data['pc_uke_nr_neu'],
                'modell' => $data['pc_modell'],
                'mac' => $data['pc_mac'],
                'serien_nr' => $data['pc_seriennr'],
                'vorname' => $data['vorname'],
                'nachname' => $data['nachname'],
                'telefon' => $data['telefon'],
                'email' => $data['email'],
                'klin_inst' => $data['klin_inst'],
                'gebaeude' => $data['gebaeude'],
                'etage' => $data['etage'],
                'raum' => $data['raum'],
                'netz' => $data['netz'],
                'ud_nr' => null,
                'pr_nr' => null
            ];
            $sent++;

            if ($model->geraetExists($geraet['uke_nr'])) {
                $ret = $model->updateGeraet($geraet);
            } else {
                $ret = $model->insertGeraet($geraet);
            }

            if ($ret == 0) {
                array_push($messages, 'cb_pc konnte nicht eingefügt werden.<br />');
            }

            $received += $ret;
            
            $lastgeraet_id = $model->getGeraet($geraet)['id'];
        }

        if ($data['cb_mon']) {
            $geraet = [
                'typ' => 3,
                'uke_nr' => $data['mon_uke_nr_neu'],
                'modell' => $data['mon_modell'],
                'vorname' => $data['vorname'],
                'nachname' => $data['nachname'],
                'telefon' => $data['telefon'],
                'email' => $data['email'],
                'klin_inst' => $data['klin_inst'],
                'gebaeude' => $data['gebaeude'],
                'etage' => $data['etage'],
                'raum' => $data['raum'],
                'netz' => $data['netz'],
                'mac' => null,
                'serien_nr' => null,
                'ud_nr' => null,
                'pr_nr' => null
            ];

            $sent++;

            if ($model->geraetExists($geraet['uke_nr'])) {
                $ret = $model->updateGeraet($geraet);
            } else {
                $ret = $model->insertGeraet($geraet);
            }

            if ($ret == 0) {
                array_push($messages, 'cb_mon konnte nicht eingefügt werden.<br />');
            }
            $received += $ret;
        }

        if ($data['cb_alt']) {
            $geraet = [
                'typ' => 0,
                'uke_nr' => $data['alt_pc_uke_nr'],
                'modell' => 'Altgerät PC/Laptop',
                'vorname' => $data['vorname'],
                'nachname' => $data['nachname'],
                'telefon' => $data['telefon'],
                'email' => $data['email'],
                'klin_inst' => $data['klin_inst'],
                'gebaeude' => $data['gebaeude'],
                'etage' => $data['etage'],
                'raum' => $data['raum'],
                'netz' => $data['netz'],
                'mac' => null,
                'serien_nr' => null,
                'ud_nr' => null,
                'pr_nr' => null
            ];

            $sent++;

            if ($model->geraetExists($geraet['uke_nr'])) {
                $ret = $model->updateGeraet($geraet);
            } else {
                $ret = $model->insertGeraet($geraet);
            }

            if ($ret == 0) {
                array_push($messages, 'cb_alt -> pc konnte nicht eingefügt werden.<br />');
            }
            $received += $ret;

            $geraet['modell'] = 'Altgerät Monitor';
            $geraet['uke_nr'] = $data['alt_mon_uke_nr'];
            $geraet['typ'] = 3;

            $sent++;
            
            if ($model->geraetExists($geraet['uke_nr'])) {
                $ret = $model->updateGeraet($geraet);
            } else {
                $ret = $model->insertGeraet($geraet);
            }

            if ($ret == 0) {
                array_push($messages, 'cb_alt -> mon konnte nicht eingefügt werden.<br />');
            }
            $received += $ret;
        }

        if ($data['cb_canon']) {
            $geraet = [
                'typ' => 4,
                'uke_nr' => null,
                'modell' => $data['pr_modell'],
                'vorname' => $data['vorname'],
                'nachname' => $data['nachname'],
                'telefon' => $data['telefon'],
                'email' => $data['email'],
                'klin_inst' => $data['klin_inst'],
                'gebaeude' => $data['gebaeude'],
                'etage' => $data['etage'],
                'raum' => $data['raum'],
                'netz' => $data['netz'],
                'mac' => null,
                'serien_nr' => null,
                'ud_nr' => null,
                'pr_nr' => $data['pr_nr']
            ];

            $sent++;
            
            if ($model->geraetExists($geraet['uke_nr'])) {
                $ret = $model->updateGeraet($geraet);
            } else {
                $ret = $model->insertGeraet($geraet);
            }

            if ($ret == 0) {
                array_push($messages, 'cb_canon konnte nicht eingefügt werden.<br />');
            }
            $received += $ret;
        }

        if (is_array($ang_ger)) {
            foreach ($ang_ger as $val) {
                $val['uke_nr'] = utils::clearText($val['uke_nr']);
                $val['bez'] = utils::clearText($val['bez']);
                $val['ud_nr'] = utils::clearText($val['ud_nr']);
            }

            foreach ($ang_ger as $ag) {
                $geraet = [
                    'typ' => 4,
                    'uke_nr' => $ag['uke_nr'],
                    'modell' => $ag['bez'],
                    'vorname' => $data['vorname'],
                    'nachname' => $data['nachname'],
                    'telefon' => $data['telefon'],
                    'email' => $data['email'],
                    'klin_inst' => $data['klin_inst'],
                    'gebaeude' => $data['gebaeude'],
                    'etage' => $data['etage'],
                    'raum' => $data['raum'],
                    'netz' => $data['netz'],
                    'mac' => null,
                    'serien_nr' => null,
                    'ud_nr' => $ag['ud_nr'],
                    'pr_nr' => null
                ];

                $sent++;
                
                if ($model->geraetExists($geraet['uke_nr'])) {
                    $ret = $model->updateGeraet($geraet);
                } else {
                    $ret = $model->insertGeraet($geraet);
                }

                if ($ret == 0) {
                    array_push($messages, 'ang_ger ->'.$ag['bez'].' konnte nicht eingefügt werden.<br />');
                }
                $received += $ret;
            }
        }

        if (is_array($software)) {
            foreach ($software as $val) {
                $val['software_bez'] = utils::clearText($val['software_bez']);
            }

            foreach ($software as $sw) {
                $soft = [
                    'name' => $sw['software_bez']
                ];

                $lastsoft_id = $model->getLastSoftware('WHERE name =\''.$soft['name'].'\'')['id'];

                if ($lastsoft_id < 1) {

                    $sent++;

                    $ret = $model->insertSoftware($soft);
                    if ($ret == 0) {
                        array_push($messages, 'sw ->'.$sw['software_bez'].' konnte nicht eingefügt werden.<br />');
                    }
                    $received += $ret;
                }

                $lastsoft_id = $model->getLastSoftware('WHERE name =\''.$soft['name'].'\'')['id'];

                if (isset($lastgeraet_id)) {
                    $model->insertSoftwareGeraete($lastsoft_id, $lastgeraet_id);
                } else {
                    array_push($messages, 'Software konnte nicht eingetragen werden, da kein Gerät angegeben wurde.<br />');
                }
            }
        }

        if ($sent != $received) {
            array_push($messages, 'e:'.$sent-$received . ' Einträge konnten nicht gemacht werden.');
        } else if ($received < 1) {
            array_push($messages, 'Es wurden 0 Einträge eingefügt.');
        } else {
            array_push($messages, 's:Alle '.$received.' Einträge wurden erfolgerich eingefügt.');
        }

        echo json_encode($messages);

        // print_r($data);
        // print_r($soft);
        // print_r($ang_ger);
    }
}