<?php

class Employees extends Controller {
	
	public function index() {
		
	}
	
	public function create() {
		$this->auth->validatePermission(313, true);

		$post = [
			'firstname' => suglo::post('firstname'),
			'lastname' => suglo::post('lastname'),
			'device_id' => suglo::post('device_id'),
			'hours_per_week' => suglo::post('hours_per_week'),
			'workdays_per_week' => suglo::post('workdays_per_week'),
			'hiring_date' => suglo::post('hiring_date'),
			'tax_number' => suglo::post('tax_number')
		];

		#is scanner_uid or device unset? -> terminate
		if (!$post['firstname'] || !$post['lastname'] || !$post['hiring_date'] 
			|| !$post['hours_per_week'] || !$post['workdays_per_week']) {
			//echo 'e;Invalid or missing parameters.';
			new JSON([], 'error', 1001, 'Invalid or missing paramater in call.');
			exit;
		}

		$model = $this->loadModel('employees');
		
		try {
			$result = $model->insertEmployee($post);
		} catch (PDOException $e) {
			if ($e->errorInfo[1] == 1062) {
				new JSON([], ' error', 9999, 'Duplicate unique entry.');
				exit;
			} else {
				#throw $e;
				new JSON([], ' error', 9999, 'Error code'.$e->errorInfo[1].'.');
				exit;
			}
		}
		
		if ($result == 1) {
			new JSON([], 'success', 600, 'Employee entry created.');
		} else {
			new JSON([], 'error', 6001, 'Unable to insert employee.');
		}
	}
	
	public function read($id = null) {
		
		$model = $this->loadModel('employees');

		#select one spicific employee
		if ($id) {
			$this->auth->validatePermission(311, true);
			
			$employees = $model->getEmployees('WHERE employees.id = :id',
				[
					':id' => $id
				]
			);
			
		#select all employees
		} else {
			$post = [
				'device_uid' => suglo::post('device_uid')
			];
			
			$this->auth->validatePermission(310, true);

			$employees = $model->getEmployees('WHERE devices.uid = :device_uid',
				[
					':device_uid' => $post['device_uid']
				]
			);
		}

		new JSON($employees);
	}
	
	public function register() {
		$post = [
			'device_uid' => suglo::auto('device_uid'),
			'regcode' => suglo::auto('regcode')
		];

		$new_api_code = utils::createString(64);
		$employeesModel = $this->loadModel('employees');
		$devicesModel = $this->loadModel('devices');

		if (!utils::checkIsset($post)) {
			new JSON([], 'error', 3021, 'Device UID or Regcode not set.');
			exit;
		}

		#find employee id in regcodes with unused code
		$employee = $employeesModel->getEmployeeIdByRegcode($post['regcode']);

		if (!($employee > 0)) {
			new JSON([], 'error', 3022, 'Could not find regcode.');
			exit;
		}

		#find employee id with dublicate device_uid
		$dub_entry = $employeesModel->getEmployeeByDeviceRJ($post['device_uid']);

		#check if dublicate exists
		if ($dub_entry['employee_id'] > 0 && $dub_entry['employee_id'] != $employee['id']) {
			new JSON([], 'error', 3023, 'Device UID is already registered by another employee.');
			exit;
		} else if ($dub_entry['device_uid'] < 1) {
			#device doesn't exists - create it
			$result = $devicesModel->insertDevice(
				[
					'uid' => $post['device_uid'],
					'name' => 'ED'.$employee['employee_id'],
					'description' => 'Device of Employee ID '.$employee['employee_id'],
					'type_id' => 2
				]
			);
		}

		#select device_id by its uid
		#take device (id) that exists or has just been created
		$devices = $devicesModel->getDevices('WHERE uid = :device_uid',
			[	
				':device_uid' => $post['device_uid']
			]
		);

		#no device has been found or created prior
		if (!isset($devices[0])) {
			new JSON([], 'error', 9999, 'Device not created or not found');
			exit;
		}

		$device = $devices[0];

		#set new device_uid to employee
		$result = $employeesModel->updateEmployeeDevice(
			[
				'id' => $employee['employee_id'],
				'device_id' => $device['id'],
				'new_api_code' => $new_api_code
			]
		);

		if (!$result) {
			new JSON([], 'error', 3024, 'Unable to update user information.');
			exit;
		}

		#set regcode as used one
		$result = $employeesModel->updateRegcode($employee['employee_id'], $post['regcode']);

		if (!$result) {
			new JSON([], 'error', 3025, 'Unable to change code used state.');
			exit;
		} else {
			new JSON([], 'success', 300, $new_api_code);
			exit;
		}
	}
}