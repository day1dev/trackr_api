<?php

class Messages extends Controller {
    public function read($id = null) {

        $post = [
            'employee_id' => suglo::post('employee_id')
        ];

        if (!$post['employee_id']) {
            new JSON([], 'error', 1001, 'Invalid or missing paramater.');
            exit;
        }

        $messagesModel = $this->loadModel('messages');
        
        if ($id == null) {
            $messages = $messagesModel->getMessages(
                ' WHERE sender_employee_id = '. $post['employee_id']
                . ' OR recipient_employee_id = '. $post['employee_id']
            );
        } else {
            $messages = $messagesModel->getMessages(
                ' WHERE sender_employee_id = '. $post['employee_id']
                . ' OR recipient_employee_id = '. $post['employee_id']
                . ' AND messages.id = '. $id
            );
        }
        
        new JSON($messages);
    }
}
