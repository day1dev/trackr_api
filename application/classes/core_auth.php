<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require APP . 'libs/log.php';

class Auth {

	#data array mit api_code, api_token, seed, etc.
	public $data;
	public $role_id;
	private $dbh;
	public $employee_id;
	
	function __construct($dbh) {
		$this->data = [
			'api_code' => suglo::post('api_code'),
			'api_token' => suglo::post('api_token'), #hash of seed
			'seed' => suglo::post('seed'), #string der gehashed werden soll
			'device_uid' => suglo::post('device_uid')
		];
		
		$this->dbh = $dbh;
	}
	
	public function authenticate($guest = false) {

		if (!utils::checkIsset($this->data)) {
			new JSON([], 'error', 1001, 'Invalid or missing paramater in global data.');
			return false;
		}

		#find employee data by api_code and device_uid
		$employee = $this->dbh->row(
			'	SELECT 
					employees.id AS id,
					users.role_id AS role_id,
					users.id AS user_id
				FROM employees
				INNER JOIN devices
					ON devices.id = employees.device_id
				INNER JOIN users
					ON users.employee_id = employees.id
				WHERE employees.api_code = :api_code
				AND devices.uid = :device_uid
				AND devices.type_id = 2;',
			[
				':api_code' => $this->data['api_code'],
				':device_uid' => $this->data['device_uid']
			]
		);

		#couldnt find employee and not guest-mode
		if ($employee['id'] < 1 && !$guest) {
			Log::write($this->dbh, Utils::getRealIp().' - f:'.$employee['id'].' - '.$_SERVER['REQUEST_URI'], suglo::post(), $employee['user_id']);
			new JSON([], 'error', 1000, 'API code authentication failed or employee does not have user account.');
			return false;
		}

		#has role_id or guest-mode
		if ($employee['role_id'] > 0 || $guest) {
			$this->role_id = $employee['role_id'];
			$this->employee_id = $employee['id'];
		}

		Log::write($this->dbh, Utils::getRealIp().' - s:'.$employee['id'].' - '.$_SERVER['REQUEST_URI'], suglo::post(), $employee['user_id']);

		#check if token has already been used
		$token_used = $this->dbh->row(
			'	SELECT id
				FROM apitoken
				WHERE token = :api_token;',
			[
				':api_token' => $this->data['api_token']
			]
		);
		
		#if token used -> error; else insert token as used
		if (false && $token_used['id'] > 0) {
			new JSON([], 'error', 1000, 'Tokenauthentication failed.');
			return false;
		} else {
			$this->dbh->insert('apitoken', ['token' => $this->data['api_token']]);
		}

		#generate token from seed server sided
		$api_token_server = hash('sha256', 
								hash('sha256', 
									$this->data['seed'] . 'd5w9c1w4xc3') . 'f5e5c5w85c5');
		#beispiel string
		#seed=20191024
		#api_token=b71682976bf4058e2c90153559b1f1356e1bdbecef0f36770e4a8b1b5b8e4f05

		#match POST-token with server sided gen token
		if ($api_token_server != $this->data['api_token']) {
			new JSON([], 'error', 1000, 'Tokenauthentication failed.');
			return false;
		}
		
		return true;
	}
	
	#prüfen ob der nutzer eine bestimmte perission besitzt
    public function validatePermission($perm_id, $return_false_disabled = false) {
        if (!isset($this->role_id)) {
            new JSON([], 'error', 1002, "Missing role_id.");
			exit;
        }
        #dieser db query ist nicht unbedingt notwendig da wir schon beim login das anfragen
        #alternativ lieber prüfen ob die Role prop gesetzt ist
        if (isset($this->role_id)) {
            $permissions = [];
            $roles = $this->dbh->rows("SELECT permission_id FROM rolepermissions WHERE role_id = $this->role_id;");
            //$this->Role = explode(':', $role['role']);
            foreach ($roles as $role) {
                array_push($permissions, $role['permission_id']);
            }
        }
        #prüfen ob benötigte permission nun in unserem array ist
        if (in_array($perm_id, $permissions)) {
            return true;
        }
		
		if ($return_false_disabled) {
			new JSON([], 'error', 1002, "Missing Permission $perm_id.");
			exit;
		}

		return false;
    }
}