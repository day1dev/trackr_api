<?php
# Autor: Denis Karpoukhine, d.karpoukhine@uke.de
# dbh Class enthält Datenbank information sowie die Hauptfunktionen 
# zum Ansprechen der Datenbank
#
class Dbh extends PDO {
    private $stmt;
    private $error;

    #constructor methode
    function __construct() {
        #database type
        $DB_TYPE = 'mysql';
        #database host/ip
        $DB_HOST = '94.177.207.169';
        #database name
        $DB_NAME = 'ietk';
        #MySQL username
        $DB_USER = 'hkl_ietk';
        #MySQL user passwort
        $DB_PASS = '5ReoYrBiufPfI58G';
        #mySQL charset
        $DB_CHARSET = 'utf8';
        #PDO objekt erstellen und zuweisen
        parent::__construct($DB_TYPE.':host='.$DB_HOST.';dbname='.$DB_NAME.';charset='.$DB_CHARSET, $DB_USER, $DB_PASS);
        #fehler printing aktivieren (nur bei debug/developement)
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }

    # Gibt rows aus der datenbank die mit der query gefunden wurden zurück
    public function rows($query, $params = null, $format = PDO::FETCH_ASSOC) {
        #setzt den statement parameter bevor query ausgeführt wird
        $this->stmt = $this->prepare($query);
        #für query über pdo parameter ersetzung aus
        $this->stmt->execute($params);
        #fetcht die matching rows
        $rows = $this->stmt->fetchAll($format);
        #Fehler array der Fehlervariable zuweisen
        $this->error = $this->errorInfo();
        #gefetchte rows zurückgeben (Array)
        return $rows;
    }

    #macht das gleiche wie rows jedoch nur mit einer row
    public function row($query, $params = null) {
        #setzt den statement parameter bevor query ausgeführt wird
        $this->stmt = $this->prepare($query);
        #für query über pdo parameter ersetzung aus
        $this->stmt->execute($params);
        #fetcht die matching rows
        $row = $this->stmt->fetch(PDO::FETCH_ASSOC);
        #Fehler array der Fehlervariable zuweisen
        $this->error = $this->errorInfo();
        #gefetchte rows zurückgeben (Array)
        #$this->dbh->printError();
        return $row;
    }

    #gibt splate wieder
    public function column() {

    }

    /**
     * insert
     * @param string $table A name of table to insert into
     * @param string $data An associative array
     */
    public function insert($table, $data)
    {
        ksort($data);
        
        $fieldNames = implode('`, `', array_keys($data));
        $fieldValues = ':' . implode(', :', array_keys($data));
        
        $this->stmt = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");
        
        foreach ($data as $key => $value) {
            $this->stmt->bindValue(":$key", $value);
        }
        
        return $this->stmt->execute();
    }
    
    public function update($table, $data, $where)
    {
        ksort($data);
        
        $fieldDetails = NULL;
        foreach($data as $key=> $value) {
            $fieldDetails .= "`$key`=:$key,";
        }
        $fieldDetails = rtrim($fieldDetails, ',');
        
        $this->stmt = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");
        
        foreach ($data as $key => $value) {
            $this->stmt->bindValue(":$key", $value);
        }
        
        return $this->stmt->execute();
    }

    #macht das gleiche wie rows jedoch ohne rückgabe von Zeilen, stattdessen erfolgswert
    public function query($query, $params) {
        #setzt den statement parameter bevor query ausgeführt wird
        $this->stmt = $this->prepare($query);
        #für query über pdo parameter ersetzung aus
		try {
			$result = $this->stmt->execute($params);
		} catch (Exception $e) {
			#echo 'e;'.$e->getMessage();
			return false;
		}
        #Fehler array zuweisen
        $this->error = $this->errorInfo();
        #erfolgswert zurückgeben
        return $result;
    }

    #gibt fehler auf bildschirm aus
    public function printError() {
        print_r($this->error);
    }

    #gibt fehler wert zurück
    public function getError() {
        return $this->error;
    }
}