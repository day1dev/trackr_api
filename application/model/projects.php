<?php

class ProjectsModel extends Model {
	public function insertProject($project) {
		return $this->dbh->insert('projects',
			[
				'name' => $project['name'],
				'description' => $project['description'],
				'plan_from' => $project['plan_from'],
				'plan_to' => $project['plan_to'],
				'plan_hours' => $project['plan_hours']
			]
		);
	}
	
    public function getProjects($cond = null, $params = null, $paging = null) {
        return $this->dbh->rows("
                    SELECT DISTINCT	
						projects.name AS project_name,
						projects.id AS project_id,
						projects.status_id AS status_id,
						status.name AS status
					FROM projects
					LEFT JOIN employeeprojects
						ON projects.id = employeeprojects.project_id
					LEFT JOIN employees
						ON employees.id = employeeprojects.employee_id
					INNER JOIN status
						ON status.id = projects.status_id ".$cond.
					($paging != null ? 'LIMIT '.$paging['offset'].', '.$paging['rows'] : '').
					";", 
					$params
				);
    }
}