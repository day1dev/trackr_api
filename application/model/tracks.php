<?php

class TracksModel extends Model {
	public function insertTrack($track) {
		return $this->dbh->insert('tracks',
			[
				'employee_id' => $track['employee_id'],
				'device_id' => $track['device_id'],
				'status_id' => $track['new_status_id'],
				'project_id' => $track['project_id'] != null ? $track['project_id'] : null,
				'date' => $track['timestamp'] != null ? $track['timestamp'] : null
			]
		);
	}
	
	public function getPrevTrack($employee_id) {
		return $this->dbh->row(
			"	SELECT 	status.id AS status_id, 
						status.name AS status_name,
						tracks.date AS date
				FROM tracks
				LEFT JOIN status
					ON status.id = tracks.status_id 
				WHERE tracks.employee_id = :employee_id
					AND DATE(tracks.date) <= CURDATE()
				ORDER BY tracks.id DESC 
				LIMIT 1;",
				[
					':employee_id' => $employee_id
				]
		);
	}
	
    public function getTracks($cond = null, $params = null, $paging = null) {
        return $this->dbh->rows("
		SELECT	tracks.id AS track_id,
				device_track.uid AS track_device_uid,
				tracks.status_id AS status_id,
				tracks.project_id AS project_id,
				tracks.date AS timestamp,
				projects.name AS project_name
		FROM tracks 
		INNER JOIN employees
			ON employees.id = tracks.employee_id
		INNER JOIN devices device_track
			ON device_track.id = tracks.device_id
		LEFT JOIN projects
			ON projects.id = tracks.project_id ".$cond.
					($paging != null ? 'LIMIT '.$paging['offset'].', '.$paging['rows'] : '').
					" ORDER BY track_id DESC;", 
					$params
				);
    }
}