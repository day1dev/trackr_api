<?php

class EmployeesModel extends Model {
	public function insertEmployee($employee) {
		return $this->dbh->insert('employees',
			[
				'firstname' => $employee['firstname'],
				'lastname' => $employee['lastname'],
				'device_id' => $employee['device_id'] <= 0 ? null : $employee['device_id'],
				'hours_per_week' => $employee['hours_per_week'],
				'workdays_per_week' => $employee['workdays_per_week'],
				'hiredate' => $employee['hiring_date'],
				'tax_number' => $employee['tax_number'],
				'api_code' => utils::createString(64)
			]
		);
	}
	
    public function getEmployees($cond = null, $params = null, $paging = null) {
        return $this->dbh->rows("
                    SELECT
						employees.id AS id,
						employees.firstname AS firstname,
						employees.lastname AS lastname,
						employees.hours_per_week AS hours_per_week,
						employees.workdays_per_week AS workdays_per_week,
						employees.hiredate AS hiring_date,
						employees.tax_number AS tax_number,
						employees.status_id AS status_id,
						employees.api_code AS api_code,
						users.role_id AS role_id,
						roles.name AS role_name
					FROM employees
					INNER JOIN users
						ON users.employee_id  = employees.id
					LEFT JOIN devices
						ON devices.id = employees.device_id
					INNER JOIN roles
						ON roles.id = users.role_id ".$cond.
					($paging != null ? 'LIMIT '.$paging['offset'].', '.$paging['rows'] : '').
					";", 
					$params
				);
    }
	
	public function getEmployeeByDevice($device_uid) {
		return $this->dbh->row('
				SELECT employees.id AS id
				FROM employees
				INNER JOIN devices
					ON employees.device_id = devices.id
				WHERE devices.uid = :device_uid;',
				[
					':device_uid' => $device_uid
				]
		);
	}

	#Right Joined get employee by device
	public function getEmployeeByDeviceRJ($device_uid) {
		return $this->dbh->row('
				SELECT 	employees.id AS employee_id, 
						devices.uid AS device_uid
				FROM employees
				RIGHT JOIN devices
					ON employees.device_id = devices.id
				WHERE devices.uid = :device_uid;',
			[
				':device_uid' => $device_uid
			]
		);
	}
	
	public function updateEmployeeDevice($employee) {
		return $this->dbh->update('employees',
			[
				'device_id' => $employee['device_id'],
				'api_code' => $employee['new_api_code']
			],
			'id = '.$employee['id']
		);
	}
	
	public function updateRegcode($employee_id, $regcode) {
		return $this->dbh->update('regcodes',
			[
				'used' => 1
			],
			'employee_id = '.$employee_id.' AND code =\''.$regcode.'\''
		);
	}

	public function getEmployeeIdByRegcode($regcode) {
		return $this->dbh->row('
			SELECT employee_id 
				FROM regcodes 
				WHERE employee_id != 0 
				AND code = :regcode 
				AND used = 0;',
				[
					':regcode' => $regcode
				]
		);

	}
}