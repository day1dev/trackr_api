<?php

class DevicesModel extends Model {
	public function insertDevice($device) {
		return $this->dbh->insert('devices',
			[
				'name' => $device['name'],
				'description' => $device['description'],
				'uid' => $device['uid'],
				'type_id' => $device['type_id']
			]
		);
	}
	
    public function getDevices($cond = null, $params = null, $paging = null) {
        return $this->dbh->rows("
                    SELECT 	devices.id AS id,
							devices.name AS name,
							devices.description AS description,
							devices.uid AS uid,
							types.name AS type,
							types.id AS type_id
                    FROM devices
					INNER JOIN types ON devices.type_id = types.id ".$cond.
					($paging != null ? 'LIMIT '.$paging['offset'].', '.$paging['rows'] : '').
					";", 
					$params
        );
    }
}