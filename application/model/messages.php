<?php

class MessagesModel extends Model {
    #alle messages holen aus der db
    public function getMessages($cond = null, $params = null, $paging = null) {
        return $this->dbh->rows("
                    SELECT 	messages.id AS id, 
                            messages.subject AS subject,
                            messages.body AS body,
                            messages.sender_employee_id AS sender_id,
                            sender.firstname AS sender_firstname,
                            sender.lastname AS sender_lastname,
                            messages.recipient_employee_id AS recipient_id,
                            recipient.firstname AS recipient_firstname,
                            recipient.lastname AS recipient_lastname,
							messages.sent_date AS sent_date,
							messages.status_id AS status_id,
							status.name AS status
					FROM messages
                    LEFT JOIN employees sender ON messages.sender_employee_id = sender.id
                    LEFT JOIN employees recipient ON messages.recipient_employee_id = recipient.id
					INNER JOIN status ON messages.status_id = status.id ".$cond.
					" ORDER BY messages.id DESC ".
					($paging != null ? 'LIMIT '.$paging['offset'].', '.$paging['rows'] : '').
					";",
					$params
        );
    }
}
