<?php

class PermissionsModel extends Model {
    public function getPermissions($params = null) {
        return $this->dbh->rows(
            "SELECT DISTINCT
                permissions.id AS id,
                permissions.name AS name,
                --rolepermissions.role_id AS role
            FROM permissions 
            INNER JOIN rolepermissions 
                ON rolepermissions.permission_id = permissions.id
            WHERE rolepermissions.role_id = :role_id
        UNION
            SELECT DISTINCT
                permissions.id AS id,
                permissions.name AS name,
                rolepermissions.role_id AS role
            FROM permissions 
            INNER JOIN rolepermissions 
                ON rolepermissions.permission_id = permissions.id
            INNER JOIN roles 
                ON roles.inherited_id = rolepermissions.role_id
            WHERE roles.id = :role_id2
            GROUP BY permissions.id ;"
            , $params
        );
    }
}