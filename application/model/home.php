<?php
class HomeModel extends Model {

    public function insertGeraet($dat) {
        return $this->dbh->query(
            'INSERT INTO geraete (
                typ,
                modell,
                serien_nr,
                mac,
                uke_nr,
                ud_nr,
                pr_nr,
                netz,
                klin_inst,
                gebaeude,
                etage,
                raum,
                vorname,
                nachname,
                telefon,
                email
            ) VALUES (
                :typ,
                :modell,
                :serien_nr,
                :mac,
                :uke_nr,
                :ud_nr,
                :pr_nr,
                :netz,
                :klin_inst,
                :gebaeude,
                :etage,
                :raum,
                :vorname,
                :nachname,
                :telefon,
                :email
            );',
            [
                ':typ' => $dat['typ'],
                ':modell' => $dat['modell'],
                ':serien_nr' => $dat['serien_nr'],
                ':mac' => $dat['mac'],
                ':uke_nr' => $dat['uke_nr'],
                ':ud_nr' => $dat['ud_nr'],
                ':pr_nr' => $dat['pr_nr'],
                ':netz' => $dat['netz'],
                ':klin_inst' => $dat['klin_inst'],
                ':gebaeude' => $dat['gebaeude'],
                ':etage' => $dat['etage'],
                ':raum' => $dat['raum'],
                ':vorname' => $dat['vorname'],
                ':nachname' => $dat['nachname'],
                ':telefon' => $dat['telefon'],
                ':email' => $dat['email'],
            ]
        );
    }

    public function updateGeraet($dat) {
        return $this->dbh->query(
            'UPDATE geraete
            SET
                typ = :typ,
                modell = :modell,
                serien_nr = :serien_nr,
                mac = :mac,
                uke_nr = :uke_nr,
                ud_nr = :ud_nr,
                pr_nr = :pr_nr,
                netz = :netz,
                klin_inst = :klin_inst,
                gebaeude = :gebaeude,
                etage = :etage,
                raum = :raum,
                vorname = :vorname,
                nachname = :nachname,
                telefon = :telefon,
                email = :email
            WHERE uke_nr = :uke_nr2;',
            [
                ':typ' => $dat['typ'],
                ':modell' => $dat['modell'],
                ':serien_nr' => $dat['serien_nr'],
                ':mac' => $dat['mac'],
                ':uke_nr' => $dat['uke_nr'],
                ':uke_nr2' => $dat['uke_nr'],
                ':ud_nr' => $dat['ud_nr'],
                ':pr_nr' => $dat['pr_nr'],
                ':netz' => $dat['netz'],
                ':klin_inst' => $dat['klin_inst'],
                ':gebaeude' => $dat['gebaeude'],
                ':etage' => $dat['etage'],
                ':raum' => $dat['raum'],
                ':vorname' => $dat['vorname'],
                ':nachname' => $dat['nachname'],
                ':telefon' => $dat['telefon'],
                ':email' => $dat['email']
            ]
        );
    }
	
	public function insertNetz($netz) {
		return $this->dbh->query(
			'INSERT INTO netze (
				name
			) VALUES (
				:netz
			);',
			[
				':netz' => $netz
			]
		);
	}
	
	public function getLastNetz($where) {
		return $this->dbh->row(
            'SELECT id FROM netze ' . $where . ' ORDER BY id DESC LIMIT 1;'
        );
	}

    public function insertSoftware($soft) {
        return $this->dbh->query(
            'INSERT INTO software (
                name
            ) VALUES (
                :name
            );',
            [
                ':name' => $soft['name']
            ]
        );
    }

    public function insertSoftwareGeraete($soft_id, $geraet_id) {
        return $this->dbh->query(
            'INSERT INTO software_geraete (
                software_id,
                geraet_id
            ) VALUES (
                :software_id,
                :geraet_id
            )',
            [
                ':software_id' => $soft_id,
                ':geraet_id' => $geraet_id
            ]
        );
    }

    public function getLastSoftware($where) {
        return $this->dbh->row(
            'SELECT id FROM software ' . $where . ' ORDER BY id DESC LIMIT 1;'
        );
    }

    public function geraetExists($uke_nr) {
        return $this->dbh->row(
            'SELECT id FROM geraete WHERE uke_nr = :uke_nr;',
            [
                ':uke_nr' => $uke_nr
            ]
            )['id'] > 0;
    }

    public function getGeraet($dat) {
        return $this->dbh->row(
            'SELECT * FROM geraete WHERE uke_nr = :pc_uke_nr_neu;',
            [
                ':pc_uke_nr_neu' => $dat['uke_nr']
            ]
        );
    }
}