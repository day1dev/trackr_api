<?php

class StatusModel extends Model {
	
    public function getStatus($cond = null, $params = null, $paging = null) {
        return $this->dbh->rows("
                    SELECT name 
					FROM status ".$cond.
					($paging != null ? 'LIMIT '.$paging['offset'].', '.$paging['rows'] : '').
					";", 
					$params
				);
    }
}