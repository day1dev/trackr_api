<?php

class VacationsModel extends Model {
	public function insertVacation($vacation) {
		return $this->dbh->insert('vacations',
			[
				'employee_id' => $vacation['employee_id'],
				'from' => $vacation['date_from'],
				'to' => $vacation['date_to']
			]
		);
	}
	
	public function updateVacation($params, $cond) {
		return $this->dbh->update('vacations', $params, $cond);
	}
	
    public function getVacations($cond = null, $params = null, $paging = null) {
        return $this->dbh->rows("
                    SELECT
						vacations.id AS id,
						vacations.from AS `from`,
						vacations.to AS `to`,
						vacations.status_id AS status_id,
						status.name AS status
					FROM vacations
					INNER JOIN employees
						ON vacations.employee_id = employees.id
					LEFT JOIN devices
						ON devices.id = employees.device_id
					INNER JOIN status
						ON vacations.status_id = status.id ".$cond.
					($paging != null ? 'LIMIT '.$paging['offset'].', '.$paging['rows'] : '').
					";", 
					$params
        );
    }
}