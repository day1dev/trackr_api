<?php 
# Autor: Denis Karpoukhine

define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
// set a constant that holds the project's "application" folder, like "/var/www/application".
define('APP', ROOT . 'application' . DIRECTORY_SEPARATOR);
define('PUB', ROOT . 'public' . DIRECTORY_SEPARATOR);
// This is the (totally optional) auto-loader for Composer-dependencies (to load tools into your project).
// If you have no idea what this means: Don't worry, you don't need it, simply leave it like it is.
if (file_exists(ROOT . 'vendor/autoload.php')) {
    require ROOT . 'vendor/autoload.php';
}
// load application config (error reporting etc.)
require APP . 'config/config.php';
// FOR DEVELOPMENT: this loads PDO-debug, a simple function that shows the SQL query (when using PDO).
// If you want to load pdoDebug via Composer, then have a look here: https://github.com/panique/pdo-debug
require APP . 'libs/helper.php';
// load application class
require APP . 'core/application.php';
require APP . 'core/controller.php';
require APP . 'core/model.php';
require APP . 'libs/suglo.php';
require APP . 'libs/utils.php';
require APP . 'libs/json.php';
require APP . 'libs/permissions.enum.php';

$app = new Application();

?>