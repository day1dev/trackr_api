/*  Denis Karpoukhine
**  GB-IT, UKE
**  2019
*/
$(document).ready(function () {
    $('input#cb-pc').prop('checked', false);
    $('input#cb-alt').prop('checked', false);
    $('input#cb-mon').prop('checked', false);
    $('input#cb-canon-drucker').prop('checked', false);
    $('select#netz').val('-1');
});

$('select#netz').on('change', function() {
    $('div#view-geraete').attr('hidden', false);
    $('div#view-software').attr('hidden', false);
    $('div#view-ang-geraete').attr('hidden', false);
    $('div#view-person').attr('hidden', false);
    $('div#view-button').attr('hidden', false);
    
	if ($('select#netz').val() == 5) {
		$('div#abteilungsnetz').prop('hidden', false);
	} else {
		$('div#abteilungsnetz').prop('hidden', true);
	}
});

$('input#cb-pc').on('change', function() {
    if ($(this).is(':checked')) {
        $('div#view-pc').attr('hidden', false);
    } else {
        $('div#view-pc').attr('hidden', true);
    }
});

$('input#cb-mon').on('change', function() {
    if ($(this).is(':checked')) {
        $('div#view-mon').attr('hidden', false);
    } else {
        $('div#view-mon').attr('hidden', true);
    }
});

$('input#cb-alt').on('change', function() {
    if ($(this).is(':checked')) {
        $('div#view-alt').attr('hidden', false);
    } else {
        $('div#view-alt').attr('hidden', true);
    }
});

$('input#cb-canon-drucker').on('change', function() {
    if ($(this).is(':checked')) {
        $('div#view-canon-drucker').attr('hidden', false);
    } else {
        $('div#view-canon-drucker').attr('hidden', true);
    }
});

$('section').on('click', 'button.add-software', function() {
    // var sel_item = $(this).val();

    // if (sel_item < 1) {
    //     return;
    // }

    // if ($(this).attr('touched')) {
    //     return;
    // }

    // $(this).attr('touched', true);

    $(this).parents('div.software').append(
    '<div class="row my-md-2 software_neu xtra">'+
        '<div class="col">'+
            '<input name="software_neu" class="software_bez form-control" type="text" placeholder="Software" maxlength="20" />'+
            // '<select name="software_neu" class="software_bez form-control" required>'+
                // '<option value="-1">Software Auswählen...</option>'+
                // '<option value="1">Photoshop CC 2019</option>'+
                // '<option value="2">Microsoft Office 2019</option>'+
            // '</select>'+
        '</div>'+
        '<div class="col">'+
            '<button class="btn btn-danger delete-software">X</button>'+
        '</div>'+
    '</div>');
});

$('section').on('click', 'button.delete-software', function() {
    $(this).parents('div.row').remove();
});


$('section').on('click', 'button.add-ang-geraet', function() {
    // var sel_item = $(this).val();

    // if (sel_item < 1) {
    //     return;
    // }

    // if ($(this).attr('touched')) {
    //     return;
    // }

    // $(this).attr('touched', true);

    $(this).parents('div.ang-geraete').append(
    '<div class="row my-md-2 ang-geraet xtra">'+
            '<div class="col">'+
                '<input name="ang_geraet_uke_nr" class="ang_geraet_uke_nr form-control" type="text" placeholder="UKE Nummer" maxlength="20" required />'+
            '</div>'+
            '<div class="col">'+
                '<input name="ang_geraet_bez" class="ang_geraet_bez form-control" type="text" placeholder="Gerätebezeichnung" maxlength="20" required />'+
            '</div>'+
            '<div class="col">'+
                '<input name="ud_nr" class="ud_nr form-control" type="text" placeholder="UD Nummer" maxlength="20" required />'+
            '</div>'+
            '<div class="col">'+
                '<button class="btn btn-danger delete-ang-geraet">X</button>'+
            '</div>'+
        '</div>'
    );
});

$('section').on('click', 'button.delete-ang-geraet', function() {
    $(this).parents('div.row').remove();
});

$('section').on('click', 'button.delete-software', function() {
    $(this).parents('div.row').remove();
});

$('section').on('click', 'div.alert', function() {
    $(this).remove();
});

$('#clear').on('click', function() {
        $('select#netz').val('');
        // Neugerät
        $('select#pc_typ').val('-1');
        $('input#pc_uke_nr_neu').val('');
        $('input#pc_modell').val('');
        $('input#pc_mac').val('');
        $('input#pc_seriennr').val('');
        // Monitor
        $('input#mon_uke_nr_neu').val('');
        $('input#mon_modell').val('');
        // Algerät
        $('input#alt_pc_uke_nr').val('');
        $('input#alt_mon_uke_nr').val('');
        // Drucker
        $('input#pr_nr').val('');
        $('input#pr_modell').val('');
        //Person
        $('input#vorname').val('');
        $('input#nachname').val('');
        $('input#telefon').val('');
        $('input#email').val('');
        $('input#klin_inst').val('');
        $('input#gebaeude').val('');
        $('select#etage').val('-1');
        $('input#raum').val('');
        //Angeschlossene Geräte und Software
        $('input.ang_geraet_uke_nr').val('');
        $('input.ang_geraet_bez').val('');
        $('input.ud_nr').val('');
        $('input.software_bez').val('');
        //Entfernt extra rows von ang_ger und software
        $('div.xtra').empty();

});

$('#submit').on('click', function() {
    //Array mit Daten
    var dat = {
        'netz' : $('select#netz').val(),
        // Checkboxen
        'cb_pc' : $('input#cb-pc').is(':checked') | 0,
        'cb_mon' : $('input#cb-mon').is(':checked') | 0,
        'cb_alt' : $('input#cb-alt').is(':checked') | 0,
        'cb_canon' : $('input#cb-canon-drucker').is(':checked') | 0,
        // Neugerät
        'pc_typ' : $('select#pc_typ').val(),
        'pc_uke_nr_neu' : $('input#pc_uke_nr_neu').val(),
        'pc_modell' : $('input#pc_modell').val(),
        'pc_mac' : $('input#pc_mac').val(),
        'pc_seriennr' : $('input#pc_seriennr').val(),
        // Monitor
        'mon_uke_nr_neu' : $('input#mon_uke_nr_neu').val(),
        'mon_modell' : $('input#mon_modell').val(),
        // Algerät
        'alt_pc_uke_nr' : $('input#alt_pc_uke_nr').val(),
        'alt_mon_uke_nr' : $('input#alt_mon_uke_nr').val(),
        // Drucker
        'pr_nr' : $('input#pr_nr').val(),
        'pr_modell' : $('input#pr_modell').val(),
        //Person
        'vorname' : $('input#vorname').val(),
        'nachname' : $('input#nachname').val(),
        'telefon' : $('input#telefon').val(),
        'email' : $('input#email').val(),
        'klin_inst' : $('input#klin_inst').val(),
        'gebaeude' : $('input#gebaeude').val(),
        'etage' : $('select#etage').val(),
        'raum' : $('input#raum').val(),
    };

    //Vollständigkeitscheck
    if (dat['netz'] < 1) {
        showAlert('e:Netz ist nicht gewählt.');
        return;
    }
	if (dat['netz'] == 5) {
		var abt_netz = $('input#netz_bez').val();
		
		if (abt_netz.length < 1) {
			showAlert('e:Abteilungsnetz Bezeichnung ist nicht angegeben.');
			return;
		}
		
		dat['netz'] = abt_netz;
	}
    if (
        dat['cb_pc'] && 
            (
                dat['pc_typ'].trim() <= 0 ||
                dat['pc_uke_nr_neu'].trim() == '' ||
                dat['pc_modell'].trim() == '' ||
                dat['pc_mac'].trim() == '' ||
                dat['pc_seriennr'].trim() == ''
            )
        ) {
            showAlert('e:Fehlende oder fehlerhafte Eingabe im Bereich \'Laptop/PC Neugerät\'.');
            return;
    }
    if (
        dat['cb_mon'] && 
            (
                dat['mon_uke_nr_neu'].trim() == '' ||
                dat['mon_modell'].trim() == ''
            )
        ) {
            showAlert('e:Fehlende oder fehlerhafte Eingabe im Bereich \'Monitor Neugerät\'.');
            return;
    }
    if (
        dat['cb_alt'] && 
            (
                dat['alt_pc_uke_nr'].trim() == '' ||
                dat['alt_mon_uke_nr'].trim() == ''
            )
        ) {
            showAlert('e:Fehlende oder fehlerhafte Eingabe im Bereich \'Altgeräte\'.');
            return;
    }
    if (
        dat['cb_canon'] &&
            (
                dat['pr_nr'].trim() == '' ||
                dat['pr_modell'].trim() == ''
            )
        ) {
            showAlert('e:Fehlende oder fehlerhafte Eingabe im Bereich \'Canon Drucker\'.');
            return;
    }
    if (
        dat['vorname'].trim() == '' ||
        dat['nachname'].trim() == '' ||
        dat['telefon'].trim() == '' ||
        dat['email'].trim() == '' ||
        dat['klin_inst'].trim() == '' ||
        dat['gebaeude'].trim() == '' ||
        dat['etage'].trim() <= 0 ||
        dat['raum'].trim() == ''
        ) {
            showAlert('e:Fehlende oder fehlerhafte Eingabe im Bereich \'Benutzer\'.');
            return;
    }

    //angbundene Geräte Array
    var ang_ger = $('div.ang-geraet');
    var ang_ger_f = [];

    $.each(ang_ger, function(index, element) {
        if ($(element).find('.ang_geraet_uke_nr').val().length > 0) {
            ang_ger_f.push({
                'uke_nr':$(element).find('.ang_geraet_uke_nr').val(),
                'bez':$(element).find('.ang_geraet_bez').val(),
                'ud_nr':$(element).find('.ud_nr').val()
            });
        }
    });

    //software Array
    var soft_neu = $('div.software_neu');
    var soft_neu_f = [];

    $.each(soft_neu, function(index, element) {
        if ($(element).find('.software_bez').val().length > 0) {
            soft_neu_f.push({
                'software_bez':$(element).find('.software_bez').val()
            });
        }
    });

    // console.log(soft_neu_f);
    // console.log(ang_ger_f);
    // console.log(dat);

    //POST Request
    $.post(url+'home/ajaxSubmitForm', 
        { 
            data: dat,
            soft: soft_neu_f,
            ang_ger: ang_ger_f
        }, function( data ) {
        console.log( data );
        try {
        var arr = JSON.parse(data);
        } catch (e) {
            console.log('Invalid input. Not a JSON format.');
            return;
        }
        
        $('div#execution-status').empty();
        arr.forEach(function(element) {
            showAlert(element);
        });
      });
});