function showAlert(data, obj = $('div#execution-status')) {
    var spl = data.split(":");
    switch(spl[0].trim()) {
        //fehler
        case "e":
            obj.append('<div class="alert alert-danger" role="alert">'+spl[1]
            +'<script>$(\'div.alert\');</script></div>');
            break;
        //warnung
        case "w":
            //alert();
            obj.append('<div class="alert alert-warning" role="alert">'+spl[1]
            +'<script>$(\'div.alert\');</script></div>');
            break;
        //erfolg
        case "s":
            obj.append('<div class="alert alert-success" role="alert">'+spl[1]
            +'<script>$(\'div.alert\').delay(10000).fadeOut();</script></div>');
            break;
        //alles andere
        default:
            //obj.html(data);
            obj.append('<div class="alert alert-warning" role="alert">'+data
            +'<script>$(\'div.alert\');</script></div>');
            break;
    }
}
		
function drawPopover(label, title, content) {
		document.write(`<a  tabindex="0" class="btn btn-sm" role="button" 
			data-toggle="popover" data-trigger="focus" title="{title}" 
			data-html="true" data-content="{content}">
                    {label}
                </a>`);
				
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function deleteClicked(objname) {
	
    var _selected = [];
    $.each($("input[name='cb_" + objname + "']:checked"), function(){
        _selected.push($(this).val());
    });

    console.log(_selected);
    console.log(JSON.stringify(_selected));
	//return;
    //prüfen ob etwas leer ist
    if (_selected === undefined || _selected.length == 0) {
        showAlert('e:Keine Elemente ausgewählt!');
        return;
    }

    $('button#delete').attr('disabled', "");

        //post request senden
        $.post(url + objname + 's/ajaxDelete', {
            selected: JSON.stringify(_selected),
        }, function (data) {
            showAlert(data);
            sleep(3000).then(() => {
                location.reload();
            })
        });
}

function uncheckAllClicked(objname, obj) {
	$('input:checkbox.cb-' + objname).prop('checked', false);
    $('input:checkbox.cb-' + objname).parent().removeClass('active');
    $('div#select_count strong').text('0');
	$(obj).prop('hidden', true);
	$('button#check-all').prop('hidden', false);
	$('button#delete').attr('disabled', true);
}

function checkAllClicked(objname, obj) {
	$('input:checkbox.cb-' + objname).prop('checked', true);
    $('input:checkbox.cb-' + objname).parent().addClass('active');
    $('div#select_count strong').text($('input:checkbox.cb-' + objname).length);
	//check all button durch uncheck all ersetzen
	$(obj).prop('hidden', true);
	$('button#uncheck-all').prop('hidden', false);
    $('button#delete').removeAttr('disabled');
}

function checkboxChanged(obj) {
	//$('div#select_count').removeAttr('hidden');
    //$(this).closest('.cb-label').removeAttribute('btn-outline-success');
    //$(this).parents('.cb-label').attr('btn-success');
    var count = $('div#select_count strong').text();

	//steuerung der visualisierung ausgewählter elemente
    if (count > 0) {
        if ($(obj).is(':checked')) {
            count++;
        } else {
            count--;
        }
    } else {
        count = 1;
    }
	
	//delete button enabled wenn mehr als 1 ausgewählt ist
	if (count > 0) {
		$('button#delete').removeAttr('disabled');
	} else {
		$('button#delete').attr('disabled', true);
	}

    $('div#select_count strong').text(count);
}